<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('menu_managers', function (Blueprint $table) {
            $table->id();
            $table->tinyInteger('parent_id')->default(0);
            $table->string('title')->nullable();
            $table->string('icon')->nullable();
            $table->enum('type', ['module', 'header', 'line', 'static']);
            $table->string('position')->nullable();
            $table->unsignedBigInteger('module_id')->nullable();
            $table->integer('sort');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('menu_managers');
    }
};

<?php

namespace Modules\Authentification\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\Authentification\Entities\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        app()['cache']->forget('spatie.permission.cache');

        $superadmin = User::create(
            [
                'name' => 'Super Admin',
                'username' => 'root',
                'email' => 'root@gmail.com',
                'active' => 1,
                'password' => bcrypt('superadmin'),
            ]
        );


        $admin = User::create(
            [
                'name' => 'Admin',
                'username' => 'admin',
                'email' => 'admin@gmail.com',
                'active' => 1,
                'password' => bcrypt('admin'),
            ]
        );

        $superadmin = User::find(1)->assignRole('Super Admin');
        $admin =  User::find(2)->assignRole('Admin');
    }
}

<?php

namespace Modules\Authentification\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Modules\Authentification\Entities\User;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class PermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Permission::insert([
            [
                'id' => 7,
                'name' => 'users-index',
                'guard_name' => 'web',
                'module' => NULL,
                'menu_id' => '4',
            ],
            [
                'id' => 8,
                'name' => 'users-create',
                'guard_name' => 'web',
                'module' => NULL,
                'menu_id' => '4',
            ],
            [
                'id' => 9,
                'name' => 'users-edit',
                'guard_name' => 'web',
                'module' => NULL,
                'menu_id' => '4',
            ],
            [
                'id' => 10,
                'name' => 'users-destroy',
                'guard_name' => 'web',
                'module' => NULL,
                'menu_id' => '4',
            ],
            [
                'id' => 11,
                'name' => 'modules-index',
                'guard_name' => 'web',
                'module' => NULL,
                'menu_id' => '5',
            ],
            [
                'id' => 14,
                'name' => 'menu-manager-index',
                'guard_name' => 'web',
                'module' => NULL,
                'menu_id' => '6',
            ],
            [
                'id' => 15,
                'name' => 'role-index',
                'guard_name' => 'web',
                'module' => NULL,
                'menu_id' => '3',
            ],
            [
                'id' => 16,
                'name' => 'role-create',
                'guard_name' => 'web',
                'module' => NULL,
                'menu_id' => '3',
            ],
            [
                'id' => 31,
                'name' => 'menu-manager-create',
                'guard_name' => 'web',
                'module' => NULL,
                'menu_id' => '6',
            ],
            [
                'id' => 33,
                'name' => 'menu-manager-edit',
                'guard_name' => 'web',
                'module' => NULL,
                'menu_id' => '6',
            ],
            [
                'id' => 34,
                'name' => 'menu-manager-destroy',
                'guard_name' => 'web',
                'module' => NULL,
                'menu_id' => '6',
            ],
            [
                'id' => 37,
                'name' => 'role-edit',
                'guard_name' => 'web',
                'module' => NULL,
                'menu_id' => '3',
            ],
            [
                'id' => 38,
                'name' => 'role-destroy',
                'guard_name' => 'web',
                'module' => NULL,
                'menu_id' => '3',
            ], [
                'id' => 45,
                'name' => 'modules-create',
                'guard_name' => 'web',
                'module' => NULL,
                'menu_id' => '5',
            ], [
                'id' => 46,
                'name' => 'modules-edit',
                'guard_name' => 'web',
                'module' => NULL,
                'menu_id' => '5',
            ], [
                'id' => 47,
                'name' => 'modules-destroy',
                'guard_name' => 'web',
                'module' => NULL,
                'menu_id' => '5',
            ], [
                'id' => 49,
                'name' => 'dashboard-index',
                'guard_name' => 'web',
                'module' => NULL,
                'menu_id' => '7',
            ]
        ]);

        $permissions = Permission::all();
        // $this->call("OthersTableSeeder");
        foreach ($permissions as $key => $value) {
            $superadmin = Role::find(1)->givePermissionTo($value->name);
            $admin = Role::find(2)->givePermissionTo($permissions);
        }
    }
}

<?php

namespace Modules\Authentification\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\Authentification\Entities\MenuManager;

class MenuManagersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        MenuManager::insert([
            [
                'id' => 3,
                'parent_id' => 8,
                'title' => 'Role',
                'icon' => NULL,
                'type' => 'module',
                'position' => NULL,
                'sort' => 1,
                'module_id' => 3,
            ],
            [
                'id' => 6,
                'parent_id' => 0,
                'title' => 'Site Settings',
                'icon' => NULL,
                'type' => 'header',
                'position' => NULL,
                'sort' => 1,
                'module_id' => NULL,
            ],
            [
                'id' => 8,
                'parent_id' => 0,
                'title' => 'Settings',
                'icon' => 'fa-solid fa-gear',
                'type' => 'static',
                'position' => NULL,
                'sort' => 2,
                'module_id' => NULL,
            ],
            [
                'id' => 9,
                'parent_id' => 8,
                'title' => 'Users',
                'icon' => NULL,
                'type' => 'module',
                'position' => NULL,
                'sort' => 2,
                'module_id' => 4,
            ],
            [
                'id' => 10,
                'parent_id' => 8,
                'title' => 'Modules',
                'icon' => NULL,
                'type' => 'module',
                'position' => NULL,
                'sort' => 3,
                'module_id' => 5,
            ],
            [
                'id' => 11,
                'parent_id' => 8,
                'title' => 'Menu Manager',
                'icon' => NULL,
                'type' => 'module',
                'position' => NULL,
                'sort' => 4,
                'module_id' => 6,
            ], [
                'id' => 12,
                'parent_id' => 0,
                'title' => 'Dashboard',
                'icon' => 'fa-solid fa-house',
                'type' => 'module',
                'position' => NULL,
                'module_id' => 7,
                'sort' => 1,
            ]
        ]);

        // $this->call("OthersTableSeeder");
    }
}

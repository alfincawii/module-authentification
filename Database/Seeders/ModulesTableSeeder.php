<?php

namespace Modules\Authentification\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\Authentification\Entities\Menu;

class ModulesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Menu::insert([
            [
                'id' => 3,
                'name' => 'Role',
                'href' => '/roles',
                'slug' => 'role',
            ],
            [
                'id' => 4,
                'name' => 'Users',
                'href' => '/users',
                'slug' => 'users',
            ],
            [
                'id' => 5,
                'name' => 'Modules',
                'href' => '/module',
                'slug' => 'modules',
            ],
            [
                'id' => 6,
                'name' => 'Menu Manager',
                'href' => '/menu-manager',
                'slug' => 'menu-manager',
            ],
            [
                'id' => 7,
                'name' => 'Dashboard',
                'href' => '/home',
                'slug' => 'dashboard',
            ]
        ]);

        // $this->call("OthersTableSeeder");
    }
}

<?php

namespace Modules\Authentification\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Spatie\Permission\Models\Role;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Role::firstOrCreate([
            'name'          =>  'Super Admin',
            'guard_name'    => 'web',
            'slug'          =>  'super-admin'
        ]);

        Role::firstOrCreate([
            'name'          => 'Admin',
            'guard_name'    => 'web',
            'slug'          => 'admin'
        ]);
    }
}

<?php

namespace Modules\Authentification\Helpers;

use Carbon\Carbon;
use File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Image;

class ImageUpload
{
  public static function profilePicture($file, $location = 'storage', $old_file = NULL, $fileName = NULL, $imagetype = NULL)
  {
    if (request()->hasFile($file)) {
      ini_set('memory_limit', '256M');
      $image_path = public_path('images');
      $file = request()->file($file);
      $ext = $file->getClientOriginalExtension();
      $fileName = Str::slug(pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME) . '_' . Carbon::now()->timestamp) . '.' . $ext;

      if (!File::isDirectory("$image_path/user/original")) {
        File::makeDirectory("$image_path/user/original", 0755, true);
      }
      if (!File::isDirectory("$image_path/user/thumbnail")) {
        File::makeDirectory("$image_path/user/thumbnail", 0755, true);
      }

      if (isset($old_file) && !empty($old_file)) {
        File::delete(["images/original/" . $old_file, "images/thumbnail/" . $old_file]);
      }

      //thumbnail
      Image::make($file)->save($image_path . '/user/thumbnail/' . $fileName)->resize(300, 300);
      File::delete("images/user/thumbnail/$old_file");
      //oriinal
      Image::make($file)->save($image_path . '/user/original/' . $fileName);
      File::delete("images/user/original/$old_file");
    }
    return $fileName;
  }
}

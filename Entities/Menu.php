<?php

namespace Modules\Authentification\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Spatie\Permission\Models\Permission;

class Menu extends Model
{
    use HasFactory;

    protected $guarded = [];

    public function permissions()
    {
        return $this->hasMany(Permission::class, 'menu_id');
    }

    protected static function newFactory()
    {
        return \Modules\Authentification\Database\factories\MenuFactory::new();
    }
}

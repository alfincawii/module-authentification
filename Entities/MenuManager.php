<?php

namespace Modules\Authentification\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class MenuManager extends Model
{
    use HasFactory;

    protected $table = 'menu_managers';
    protected $guarded = [];
    public $timestamps = false;

    public function getall()
    {
        return $this->orderBy('sort', 'asc')->get();
    }

    public function module()
    {
        return $this->hasOne(Menu::class, 'id', 'module_id');
    }

    protected static function newFactory()
    {
        return \Modules\Authentification\Database\factories\MenuManagerFactory::new();
    }
}

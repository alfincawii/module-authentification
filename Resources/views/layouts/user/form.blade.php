@extends('authentification::layouts.main')
@section('contents')
<form id="formStore" action="{{ $config['form']->action }}" method="POST">
  @method($config['form']->method)
  @csrf
  <div class="col-md-12">
    <div id="errorCreate" class="mb-3" style="display:none;">
      <div class="alert alert-danger" role="alert">
        <div class="alert-icon"><i class="flaticon-danger text-danger"></i></div>
        <div class="alert-text">
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-7">
      <div class="card">
        <div class="card-header">
          <div class="row">
            <div class="col-md-6 col-12 d-flex justify-content-center justify-content-md-start align-items-center mb-3 mb-md-0">
              <h3 class="card-title">Tambah Pengguna</h3>
            </div>
            <div class="col-md-6 col-12 d-flex justify-content-center justify-content-md-end align-items-baseline mb-md-0 mb-2">
              <div class="btn-group float-md-right" role="group" aria-label="Button group with nested dropdown">
                <a onclick="history.back()" class="btn btn-outline-primary float-end">
                  <i class="fa-solid fa-rotate-left"></i> Kembali
                </a>
                <button type="submit" class="btn btn-success text-white float-end">
                  <i class="fa-solid fa-plus"></i> Simpan
                </button>
              </div>
            </div>
          </div>
        </div>
        <div class="mb-3 row">
          <label class="control-label col-sm-3 align-self-center mb-0" for="name">Nama :</label>
          <div class="col-sm-9">
            <input type="text" class="form-control" id="name" name="name" placeholder="Masukkan Nama Anda" value="{{ $data->name ?? '' }}">
          </div>
        </div>
        <div class="mb-3 row">
          <label class="control-label col-sm-3 align-self-center mb-0" for="username">Username :</label>
          <div class="col-sm-9">
            <input type="text" class="form-control" id="username" name="username" placeholder="Masukan Username Anda" value="{{ $data->username ?? '' }}" {{ (isset($data) ? 'readonly' : '') }}>
          </div>
        </div>
        <div class="mb-3 row">
          <label class="control-label col-sm-3 align-self-center mb-0" for="email">Email :</label>
          <div class="col-sm-9">
            <input type="text" class="form-control" id="email" name="email" placeholder="Masukan Email" value="{{ $data->email ?? '' }}" {{ (isset($data) ? 'readonly' : '') }}>
          </div>
        </div>
        <div class="mb-3 row">
          <label class="control-label col-sm-3 align-self-center mb-0" for="select2Role">Role :</label>
          <div class="col-sm-9">
            <select id="select2Role" style="width: 100% !important;" name="roles[]" multiple="multiple">
              @isset($data->roles)
              @foreach ($data->roles as $item)
              <option value="{{ $item->id }}" selected="selected">{{ $item->name }}</option>
              @endforeach
              @endisset
            </select>
          </div>
        </div>
        <div style="{{ isset($data) ? 'display:none' : '' }}">
          <div class="mb-3 row">
            <label class="control-label col-sm-3 align-self-center mb-0" for="password">Password :</label>
            <div class="col-sm-9">
              <input type="password" class="form-control" name="password" id="password" placeholder="Enter Your password">
            </div>
          </div>
          <div class="mb-3 row">
            <label class="control-label col-sm-3 align-self-center mb-0" for="confirm_password">Konfirmasi Password :</label>
            <div class="col-sm-9">
              <input type="password" class="form-control" id="confirm_password" name="password_confirmation" placeholder="Konfirmasi Password">
            </div>
          </div>
        </div>
        <div class="mb-3 row">
          <label class="control-label col-sm-3 align-self-center mb-0" for="active">Status :</label>
          <div class="col-sm-9">
            <select id="active" name="active" class="form-select" placeholder="Status User ?">
              <option value="1">Aktif</option>
              <option value="0">Tidak Aktif</option>
            </select>
          </div>
        </div>
      </div>
    </div>
    <div class="col-md-5">
      <div class="card">
        <div class="card-header">
          <div class="row">
            <div class="col-md-6 col-12 d-flex justify-content-center justify-content-md-start align-items-center mb-3 mb-md-0">
              <h3 class="card-title">Upload Foto Profile</h3>
            </div>
          </div>
        </div>
        <label class="mx-0 text-bold text-center d-block mb-3">Photo</label>
        <img id="avatar" src="{{ isset($data) && $data->image != NULL ? asset('images/user/thumbnail/'.$data->image) : Module::asset('authentification:img/profile/empty_profile.png') }}" style="object-fit: cover; border: 1px solid #d9d9d9" class="mb-2 border-2 mx-auto rounded-circle" height="150px" width="150px" alt="">
        <div class="row d-flex justify-content-center m-3">
          <input type="file" id="img-input" class="image d-block form-control form-control-sm" name="image" accept=".jpg, .jpeg, .png">
          <p class="text-center ms-75 mt-50"><small>Allowed JPG, JPEG or PNG. Max
              size of
              2000kB</small></p>
        </div>
      </div>
    </div>
  </div>
</form>


@endsection

@section('css')
<link rel="stylesheet" href="{{Module::asset('authentification:vendor/select2/select2.min.css')}}">
<link rel="stylesheet" href="{{Module::asset('authentification:vendor/select2/select2-bootstrap-5-theme.min.css')}}">
@endsection

@section('scripts')
<script src="{{Module::asset('authentification:vendor/select2/select2.full.min.js')}}"></script>
<script>
  $(document).ready(function() {

    $('#select2Role').select2({
      theme: "bootstrap-5",
      dropdownParent: $('#select2Role').parent(),
      placeholder: 'Pilih Roles',
      allowClear: true,
      multiple: true,
      ajax: {
        url: `{{ route('auth.searchRole') }}`,
        dataType: 'json',
        processResults: function(response) {
          let results = $.map(response.data, function(row, index) {
            row.id = row.id;
            row.text = row.name;
            return row;
          });
          return {
            results: results,
            pagination: {
              more: (response.next_page_url != null)
            }
          };
        }
      }
    });

    $("#img-input").change(function() {
      let thumb = $('#avatar');
      if (this.files && this.files[0]) {
        let reader = new FileReader();
        reader.onload = function(e) {
          thumb.attr('src', e.target.result);
        }
        reader.readAsDataURL(this.files[0]);
      }
    });

    $("#formStore").submit(function(e) {
      e.preventDefault();
      let form = $(this);
      let btnSubmit = form.find("[type='submit']");
      let btnSubmitHtml = btnSubmit.html();
      let url = form.attr("action");
      let data = new FormData(this);
      $.ajax({
        cache: false,
        processData: false,
        contentType: false,
        type: "POST",
        url: url,
        data: data,
        beforeSend: function() {
          btnSubmit.addClass("disabled").html('<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span> Loading...').prop("disabled", "disabled");
        },
        success: function(response) {
          let errorCreate = $('#errorCreate');
          errorCreate.css('display', 'none');
          errorCreate.find('.alert-text').html('');
          btnSubmit.removeClass("disabled").html(btnSubmitHtml).removeAttr("disabled");
          if (response.status === "success") {
            toastr.success(response.message, 'Success !');
            setTimeout(function() {
              if (response.redirect === "" || response.redirect === "reload") {
                location.reload();
              } else {
                location.href = response.redirect;
              }
            }, 1000);
          } else {
            toastr.error((response.message ? response.message : "Please complete your form"), 'Failed !');
            if (response.error !== undefined) {
              errorCreate.removeAttr('style');
              $.each(response.error, function(key, value) {
                errorCreate.find('.alert-text').append('<span style="display: block">' + value + '</span>');
              });
            }
          }
        },
        error: function(response) {
          btnSubmit.removeClass("disabled").html(btnSubmitHtml).removeAttr("disabled");
          toastr.error(response.responseJSON.message, 'Failed !');
        }
      });
    });

  });
</script>
@endsection
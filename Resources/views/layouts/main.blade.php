<!DOCTYPE html>
<html class="no-js" lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>{{ $config['title'] }}</title>
    <link rel="stylesheet" href="{{Module::asset('authentification:app/styles/vendor.css')}}">
    <!-- build:css styles/main.css -->
    <link rel="stylesheet" href="{{Module::asset('authentification:app/styles/main.css')}}">
    <!-- endbuild -->
    <script src="{{Module::asset('authentification:app/scripts/modernizr.js')}}"></script>
    <link rel="stylesheet" href="{{Module::asset('authentification:vendor/bootstrap/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{Module::asset('authentification:vendor/toastr/toastr.min.css')}}">
    @yield('css')


</head>

<body>
    <div class="preloader-wrapper">
        <div class="preloader">
            <img src="{{Module::asset('authentification:img/logo.png')}}" alt="Logo">
        </div>
    </div>
    <main>
        <section class="body-admin">
            <header>
                <div class="main-header">
                    <div class="left">
                        <div class="mobile-icon">
                            <div class="menu-opener">
                                <div class="menu-opener-inner"></div>
                            </div>
                        </div>
                        <div class="logo">
                            <img src="{{Module::asset('authentification:img/logo1.png')}}" alt="Logo">
                            <h1>Gink Technology</h1>
                        </div>
                        <button class="mobile-icon-right">
                            <i class="fas fa-ellipsis-v"></i>
                        </button>
                    </div>
                    <div class="right">
                        <button class="btn btn-hide-aside" id="btn-hide-aside"><i class="fas fa-align-left"></i></button>
                        <div class="menu">
                            <ul>
                                <li>
                                    <a href="#" class="dropdown-toggle dropdown-menu-right" type="button" data-bs-toggle="dropdown">
                                        <div class="avatar"><img src="{{ isset(auth()->user()->image) && auth()->user()->image != NULL ? asset('images/user/thumbnail/'.auth()->user()->image) : Module::asset('authentification:img/profile/empty_profile.png') }}" alt=""></div>
                                        <div class="name">{{ auth()->user()->name ?? '' }}</div>
                                    </a>
                                    <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                                        <a class="dropdown-item" href="javascript:void();" onclick="event.preventDefault(); document.getElementById('logout-form').submit();""><i class=" fas fa-sign-out-alt"></i> Log Out</a>
                                    </div>
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </header>

            <div class="side-menu">
                @include('authentification::layouts.partials.sidebar')
            </div>

            <div class="body">
                <div class="content-wrapper">
                    <div class="content-header row">
                        <div class="content-header-left col-md-6 col-12">
                            <h3 class="content-header-title mb-0">{{ $config['title'] }}</h3>
                            @include('authentification::components.breadcrumbs')
                        </div>
                    </div>
                    @yield('contents')
                </div>
            </div>

            <footer>
                <div class="copyright">
                    <h1>Copyright &copy; 2022 GiNK Technology</a></h1>
                </div>
            </footer>

        </section>

        <script src="{{Module::asset('authentification:vendor/jquery/jquery-3.6.3.min.js')}}"></script>
        <script src="{{Module::asset('authentification:vendor/bootstrap/bootstrap.bundle.min.js')}}"></script>
        <script src="{{Module::asset('authentification:app/scripts/main.js')}}"></script>
        <script src="{{Module::asset('authentification:vendor/toastr/toastr.min.js')}}"></script>


        <script>
            toastr.options = {
                "closeButton": false,
                "debug": false,
                "newestOnTop": false,
                "progressBar": true,
                "positionClass": "toast-top-right",
                "preventDuplicates": true,
                "onclick": null,
                "showDuration": "300",
                "hideDuration": "500",
                "timeOut": "2000",
                "extendedTimeOut": "1000",
                "showEasing": "swing",
                "hideEasing": "linear",
                "showMethod": "fadeIn",
                "hideMethod": "fadeOut"
            }
        </script>
        @yield('scripts')
    </main>

</body>


</html>
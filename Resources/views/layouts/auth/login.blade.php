<!doctype html>
<html class="no-js" lang="">

<head>
  <meta charset="utf-8">
  <meta name="description" content="">

  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>Login</title>
  <link rel="apple-touch-icon" href="apple-touch-icon.png">
  <!-- Place favicon.ico in the root directory -->
  <link rel="stylesheet" href="{{Module::asset('authentification:app/styles/vendor.css')}}">
  <!-- build:css styles/main.css -->
  <link rel="stylesheet" href="{{Module::asset('authentification:app/styles/main.css')}}">
  <script src="{{Module::asset('authentification:app/scripts/modernizr.js')}}"></script>
  <link rel="stylesheet" href="{{Module::asset('authentification:vendor/toastr/toastr.min.css')}}">
</head>

<body>
  <div class="preloader-wrapper">
    <div class="preloader">
      <img src="{{Module::asset('authentification:img/logo.png')}}" alt="Logo">
    </div>
  </div>
  <main>
    <section class="body-login">
      <div class="container">
        <div class="d-flex justify-content-center align-items-center form-place">
          <div class="row justify-content-center align-items-center w-100">
            <div class="col-lg-5 col-md-6 d-none d-md-flex justify-content-center align-items-center">
              <img src="images/map.png" class="artwork" alt="">
            </div>
            <div class="col-lg-5 col-md-6 form-box">
              <div class="form-content">
                <div class="logo">
                  <img src="{{Module::asset('authentification:img/logo.png')}}" class="img-fluid">
                  <h1 class="text-white">GiNK Technology</h1>
                  <h2 class="mb-0">Modules Management</h2>
                </div>
                <div class="content">
                  <h1>Sign in to your account</h1>
                  <form action="{{ route('login.post') }}" id="formLogin" autocomplete="off" method="POST">
                    @csrf
                    @method('POST')
                    <div class="form-group">
                      <input type="text" class="form-control form-control-sm" name="email" required="required" placeholder="Username" loginRegex>
                    </div>
                    <div class="form-group">
                      <div class="input-group">
                        <input type="password" class="form-control form-control-sm" name="password" required="required" minlength="5" placeholder="Password" />
                        <div class="input-group-append">
                          <button class="btn btn-sm btn-outline-secondary btn-show-password" type="button"><i class="far fa-eye"></i></button>
                        </div>
                      </div>
                    </div>
                    <button type="submit" class="btn btn-sm btn-login"><i class="fas fa-sign-in-alt"></i> Login</button>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>

    <script src="{{Module::asset('authentification:vendor/jquery/jquery-3.6.3.min.js')}}"></script>
    <script src="{{Module::asset('authentification:app/scripts/vendor.js')}}"></script>
    <script src="{{Module::asset('authentification:app/scripts/main.js')}}"></script>
    <script src="{{Module::asset('authentification:vendor/toastr/toastr.min.js')}}"></script>

    <script>
      $(".btn-show-password").click(function(e) {
        e.preventDefault();
        var target = $(this).parents(".input-group").find("input");
        if (target.attr("type") == "password") {
          target.attr("type", "text");
        } else {
          target.attr("type", "password");
        }
      });

      $(function() {
        $("#formLogin").submit(function(e) {
          e.preventDefault();
          let form = $(this);
          let url = form.attr("action");
          var btnSubmit = $(this).find("[type='submit']"),
            btnSubmitHtml = btnSubmit.html();
          let btnHtml = form.html();
          let data = new FormData(this);
          let spinner = $("<i class='bx bx-hourglass bx-spin font-size-16 align-middle me-2'></i>");
          $.ajax({
            beforeSend: function() {
              btnSubmit.text(' Loading. . .').prepend(spinner).prop("disabled", "disabled");
            },
            cache: false,
            processData: false,
            contentType: false,
            type: "POST",
            url: url,
            data: data,
            headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function(response) {
              if (response.status === "success") {
                toastr.success(response.message, 'Success !');
                setTimeout(function() {
                  if (response.redirect === "" || response.redirect === "reload") {
                    location.reload();
                  } else {
                    location.href = response.redirect;
                  }
                }, 1000);
              } else {
                btnSubmit.text('Submit').html(btnSubmitHtml).removeAttr('disabled');
                toastr.error((response.message ? response.message : "Username atau password salah"), 'Login Gagal !');
                if (response.error !== undefined) {
                  errorCreate.removeAttr('style');
                  $.each(response.error, function(key, value) {
                    errorCreate.find('.alert-text').append('<span style="display: block">' + value + '</span>');
                  });
                }
              }
            },
            error: function(response) {
              toastr.error(response.responseJSON.message, 'Login Gagal !');
              btnSubmit.text('Submit').html(btnSubmitHtml).removeAttr('disabled');
            }
          });
        });
      });
    </script>
  </main>
</body>

</html>
@extends('authentification::layouts.main')
@section('contents')
<div class="row">
  <div class="col-md-6">
    <div class="card">
      <div class="card-header">
        <div class="row">
          <div class="col-md-6 col-12 d-flex justify-content-center justify-content-md-start align-items-center mb-3 mb-md-0">
            <h3 class="card-title">Menu Manager</h3>
          </div>
        </div>
      </div>
      <form id="changeHierarchy" class="formStore" action="{{ route('menu-manager.changeHierarchy') }}">
        @method('POST')
        @csrf
        <div class="dd" id="menuList">
          {!! $sortable !!}
        </div>
        <div class="card-footer d-flex justify-content-end">
          <input type="hidden" id="output" name="hierarchy" />
          <button type="submit" class="btn btn-sm btn-warning" style="display: none"><i class="fa-solid fa-floppy-disk"></i> Ubah
          </button>
        </div>
      </form>
    </div>
  </div>
  <div class="col-md-6">
    <form id="formMenumanager" action="{{ $config['form']->action }}">
      @method($config['form']->method)
      @csrf
      <div class="card">
        <div class="card-header">
          <div class="row">
            <div class="col-md-6 col-12 d-flex justify-content-center justify-content-md-start align-items-center mb-3 mb-md-0">
              <h3 class="card-title">Config</h3>
            </div>
          </div>
        </div>

        <div id="errorCreate" class="mb-3" style="display:none;">
          <div class="alert alert-danger" role="alert">
            <div class="alert-icon"><i class="flaticon-danger text-danger"></i></div>
            <div class="alert-text">
            </div>
          </div>
        </div>
        <fieldset class="mb-3">
          <div class="form-check form-check-inline">
            <input type="radio" name="type" class="form-check-input" value="module" id="exampleRadio1" {{ (!isset($data) ? 'checked' : ($data->type == 'module' ? 'checked' : '' ))  }}>
            <label class="form-check-label" for="exampleRadio1">Module</label>
          </div>
          <div class="mb-3 form-check form-check-inline">
            <input type="radio" name="type" class="form-check-input" value="header" id="exampleRadio2" {{ (isset($data->type) && $data->type == 'header') ? 'checked' : '' }}>
            <label class="form-check-label" for="exampleRadio2">Header</label>
          </div>
          <div class="mb-3 form-check form-check-inline">
            <input type="radio" name="type" class="form-check-input" value="line" id="exampleRadio3" {{ (isset($data->type) && $data->type == 'line') ? 'checked' : '' }}>
            <label class="form-check-label" for="exampleRadio3">Line</label>
          </div>
          <div class="mb-3 form-check form-check-inline">
            <input type="radio" name="type" class="form-check-input" value="static" id="exampleRadio4" {{ (isset($data->type) && $data->type == 'static') ? 'checked' : '' }}>
            <label class="form-check-label" for="exampleRadio4">Static</label>
          </div>
        </fieldset>
        <div class="mb-3" style="{{ (isset($data) && $data->type == 'line') ? 'display:none;' : '' }}">
          <label class="control-label col-sm-3 align-self-center mb-0" for="select2Module">Module :</label>
          <select id="select2Module" style="width: 100% !important;" name="module_id">
            @isset($data->module)
            <option value="{{ $data->module->id }}" selected="selected">{{ $data->module->name }}</option>
            @endisset
          </select>
        </div>
        <div class="mb-3" style="{{ (isset($data) && $data->type == 'line') ? 'display:none;' : '' }}">
          <label>Nama <span class="text-danger">*</span></label>
          <input type="text" name="title" class="form-control" placeholder="ex: Menu Manager" value="{{ (isset($data) ?? $data->title) ? $data->title : '' }}" />
        </div>
        <div class="mb-3" style="{{ (isset($data) && $data->type == 'line') ? 'display:none;' : '' }}">
          <label>Icon</label>
          <input type="text" name="icon" class="form-control" placeholder="ex: fas fa-address-card" value="{{ (isset($data) ?? $data->icon) ? $data->icon : '' }}" />
        </div>
        <div class="border-top">
          <button type="submit" class="btn btn-primary float-end mt-3">Simpan <i class="fa-solid fa-floppy-disk"></i>
          </button>
        </div>
      </div>
    </form>
  </div>
</div>

<div id="confirmDeleteModal" class="modal fade">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Hapus Menu</h4>
        <button type="button" class="btn btn-outline-danger close" data-bs-dismiss="modal"><span>&times;</span></button>
      </div>
      <div class="modal-body">
        <input type="hidden" name="id" />
        <div class="alert alert-warning">
          <strong>Peringatan !</strong> Anda yakin menghapus data ini ?
        </div>
      </div>
      <div class="modal-footer">
        <div class="btn-group">
          <button type="button" class="btn btn-danger btn-sm" data-bs-dismiss="modal">No <i class="fa fa-fw fa-ban"></i></button>
          <button type="button" id="btnDelete" class="btn btn-success btn-sm">Yes <i class="fa fa-fw fa-check"></i></button>
        </div>
      </div>
    </div>
  </div>
</div>

@endsection

@section('css')
<link rel="stylesheet" href="{{Module::asset('authentification:vendor/nestable/nestable.css')}}">
<link rel="stylesheet" href="{{Module::asset('authentification:vendor/select2/select2.min.css')}}">
<link rel="stylesheet" href="{{Module::asset('authentification:vendor/select2/select2-bootstrap-5-theme.min.css')}}">
@endsection

@section('scripts')
<script src="{{Module::asset('authentification:vendor/nestable/nestable.js')}}"></script>
<script src="{{Module::asset('authentification:vendor/select2/select2.full.min.js')}}"></script>
<script>
  $(document).ready(function() {
    let radioCreate = document.querySelectorAll('input[name="type"]');
    $('#select2Module').select2({
      theme: "bootstrap-5",
      dropdownParent: $('#select2Module').parent(),
      placeholder: 'Pilih Module',
      allowClear: true,
      ajax: {
        url: `{{ route('auth.searchModule') }}`,
        dataType: 'json',
        processResults: function(response) {
          let results = $.map(response.data, function(row, index) {
            row.id = row.id;
            row.text = row.name;
            return row;
          });
          return {
            results: results,
            pagination: {
              more: (response.next_page_url != null)
            }
          };
        }
      }
    });

    $('#menuList').nestable({
      maxDepth: 3
    }).on('change', function() {
      let json_values = window.JSON.stringify($(this).nestable('serialize'));
      $("#output").val(json_values);
      $("#changeHierarchy [type='submit']").fadeIn();
    });

    radioCreate.forEach(el => {
      el.addEventListener('change', () => {
        let module_id = document.querySelector('select[name="module_id"]').parentNode;
        let title = document.querySelector('input[name="title"]').parentNode;
        let icon = document.querySelector('input[name="icon"]').parentNode;
        if (el.checked && el.value === 'header' || el.checked && el.value === 'static') {
          // document.querySelector('#createPage').style.display = '';
          module_id.style.display = 'none';
          title.style.display = '';
          icon.style.display = '';
          title.value = '';
          icon.value = '';
        } else if (el.checked && el.value === 'line') {
          module_id.style.display = 'none';
          title.style.display = 'none';
          icon.style.display = 'none';
          title.value = '';
          icon.value = '';
        } else {
          // document.querySelector('#createPage').style.display = 'none';
          module_id.style.display = '';
          title.style.display = '';
          icon.style.display = '';
          title.children.value = '';
          icon.value = '';
        }
      });
    });

    $("#formMenumanager").submit(function(e) {
      e.preventDefault();
      let form = $(this);
      let btnSubmit = form.find("[type='submit']");
      let btnSubmitHtml = btnSubmit.html();
      let url = form.attr("action");
      let data = new FormData(this);
      $.ajax({
        cache: false,
        processData: false,
        contentType: false,
        type: "POST",
        url: url,
        data: data,
        beforeSend: function() {
          btnSubmit.addClass("disabled").html('<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span> Loading...').prop("disabled", "disabled");
        },
        success: function(response) {
          let errorCreate = $('#errorCreate');
          errorCreate.css('display', 'none');
          errorCreate.find('.alert-text').html('');
          btnSubmit.removeClass("disabled").html(btnSubmitHtml).removeAttr("disabled");
          if (response.status === "success") {
            toastr.success(response.message, 'Success !');
            setTimeout(function() {
              if (response.redirect === "" || response.redirect === "reload") {
                location.reload();
              } else {
                location.href = response.redirect;
              }
            }, 1000);
          } else {
            toastr.error((response.message ? response.message : "Please complete your form"), 'Failed !');
            if (response.error !== undefined) {
              errorCreate.removeAttr('style');
              $.each(response.error, function(key, value) {
                errorCreate.find('.alert-text').append('<span style="display: block">' + value + '</span>');
              });
            }
          }
        },
        error: function(response) {
          btnSubmit.removeClass("disabled").html(btnSubmitHtml).removeAttr("disabled");
          toastr.error(response.responseJSON.message, 'Failed !');
        }
      });
    });

    $("#changeHierarchy").submit(function(e) {
      e.preventDefault();
      let form = $(this);
      let btnSubmit = form.find("[type='submit']");
      let btnSubmitHtml = btnSubmit.html();
      let url = form.attr("action");
      let data = new FormData(this);
      $.ajax({
        beforeSend: function() {
          btnSubmit.addClass("disabled").html("<i class='bx bx-hourglass bx-spin font-size-16 align-middle me-2'></i> Loading ...").prop("disabled", "disabled");
        },
        cache: false,
        processData: false,
        contentType: false,
        type: "POST",
        url: url,
        data: data,
        success: function(response) {
          let errorCreate = $('#errorCreate');
          errorCreate.css('display', 'none');
          errorCreate.find('.alert-text').html('');
          btnSubmit.removeClass("disabled").html(btnSubmitHtml).removeAttr("disabled");
          if (response.status === "success") {
            toastr.success(response.message, 'Success !');
            setTimeout(function() {
              if (!response.redirect || response.redirect === "reload") {
                location.reload();
              } else {
                location.href = response.redirect;
              }
            }, 1000);
          } else {
            $.each(response.error, function(key, value) {
              errorCreate.css('display', 'block');
              errorCreate.find('.alert-text').append('<span style="display: block">' + value + '</span>');
            });
            toastr.error((response.message ? response.message : "Please complete your form"), 'Failed !');
          }
        },
        error: function(response) {
          btnSubmit.removeClass("disabled").html(btnSubmitHtml).removeAttr("disabled");
          toastr.error(response.responseJSON.message, 'Failed !');
        }
      });
    });

    $(".btn-delete").click(function(e) {
      var pk = $(this).data('id'),
        url = `{{ route("menu-manager.index") }}/` + pk;

      $('#confirmDeleteModal').modal('toggle');

      $('#btnDelete').click(function(e) {
        e.preventDefault();
        $.ajax({
          url: url,
          type: "DELETE",
          data: {
            _token: '{{ csrf_token() }}',
            _method: 'DELETE'
          },
          error: function(response) {
            toastr.error(response, 'Failed !');
          },
          success: function(response) {
            if (response.status === "success") {
              toastr.success(response.message, 'Success !');
              $('#confirmDeleteModal').modal('hide');;
              $('.modal-backdrop').remove();
              location.reload();
            } else {
              toastr.error((response.message ? response.message : "Please complete your form"), 'Failed !');
            }
          }
        });
      });
    });
  });
</script>
@endsection
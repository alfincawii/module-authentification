@extends('authentification::layouts.main')
@section('contents')
<form id="formStore" action="{{ $config['form']->action }}" method="POST">
  @method($config['form']->method)
  @csrf
  <div class="col-md-12">
    <div id="errorCreate" class="mb-3" style="display:none;">
      <div class="alert alert-danger" role="alert">
        <div class="alert-icon"><i class="flaticon-danger text-danger"></i></div>
        <div class="alert-text">
        </div>
      </div>
    </div>
  </div>

  <div class="row">
    <div class="col-md-12">
      <div class="card">
        <div class="card-header">
          <div class="row">
            <div class="col-md-6 col-12 d-flex justify-content-center justify-content-md-start align-items-center mb-3 mb-md-0">
              <h3 class="card-title">Tambah Module</h3>
            </div>
            <div class="col-md-6 col-12 d-flex justify-content-center justify-content-md-end align-items-baseline mb-md-0 mb-2">
              <div class="btn-group float-md-right" role="group" aria-label="Button group with nested dropdown">
                <a onclick="history.back()" class="btn btn-outline-primary float-end">
                  <i class="fa-solid fa-rotate-left"></i> Kembali
                </a>
                <button type="submit" class="btn btn-success text-white float-end">
                  <i class="fa-solid fa-plus"></i> Simpan
                </button>
              </div>
            </div>
          </div>
        </div>
        <div class="mb-3 row">
          <label class="control-label col-sm-3 align-self-center mb-0" for="name">Nama :</label>
          <div class="col-sm-9">
            <input type="text" class="form-control" id="name" name="name" placeholder="Masukan Nama Module Anda" value="{{ $data->name ?? '' }}">
          </div>
        </div>
        <div class="mb-3 row">
          <label class="control-label col-sm-3 align-self-center mb-0" for="username">URI :</label>
          <div class="col-sm-9">
            <input type="text" class="form-control" id="href" name="href" placeholder="Masukan url module" value="{{ $data->href ?? '' }}">
          </div>
        </div>
        <div class="mb-3 row">
          <label class="control-label col-sm-3 align-self-center mb-0" for="email">slug :</label>
          <div class="col-sm-9">
            <input type="text" class="form-control" id="slug" name="slug" placeholder="Masukan slug" value="{{ $data->slug ?? '' }}">
          </div>
        </div>
      </div>
    </div>
  </div>


  <div class="row">
    <div class="col-md-12">
      <div class="card">
        <div class="card-header">
          <div class="row">
            <div class="col-md-6 col-12 d-flex justify-content-center justify-content-md-start align-items-center mb-3 mb-md-0">
              <h3 class="card-title">Lits Module Permission</h3>
            </div>
            <div class="col-md-6 col-12 d-flex justify-content-center justify-content-md-end align-items-baseline mb-md-0 mb-2">
              <div class="btn-group float-md-right" role="group" aria-label="Button group with nested dropdown">
                <button type="button" class="btn btn-success text-white float-end" data-bs-toggle="modal" data-bs-target="#addPermissionModal">
                  <i class="fa-solid fa-plus"></i> Tambah Permission
                </button>
              </div>
            </div>
          </div>
        </div>
        <div class="table-responsive">
          <table id="dt" class="table table-bordered w-100">
            <thead>
              <tr>
                <th>Nama Permission</th>
                <th>Nama Guard</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody></tbody>
            </tr>
            </thead>
            <tbody></tbody>
          </table>
        </div>
      </div>
    </div>
  </div>

</form>

<div id="addPermissionModal" class="modal fade">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Tambah Permission</h4>
        <button type="button" class="btn btn-outline-danger close" data-bs-dismiss="modal"><span>&times;</span></button>
      </div>
      <div class="modal-body">
        <div class="mb-3 row">
          <label class="control-label col-sm-3 align-self-center mb-0" for="name">Nama Permission:</label>
          <div class="col-sm-9">
            <input type="text" class="form-control" id="permissionName" name="name" placeholder="Masukan Nama Permission Anda" value="">
          </div>
        </div>
        <div class="mb-3 row">
          <label class="control-label col-sm-3 align-self-center mb-0" for="username">Nama Guard :</label>
          <div class="col-sm-9">
            <input type="text" class="form-control" id="guardName" name="guard_name" placeholder="Masukan Guard" value="">
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <div class="btn-group">
          <button type="button" class="btn btn-danger btn-sm" data-bs-dismiss="modal">Cancel <i class="fa fa-fw fa-ban"></i></button>
          <button type="button" id="add-permission" class="btn btn-success btn-sm">Tambah <i class="fa fa-fw fa-check"></i></button>
        </div>
      </div>
    </div>
  </div>
</div>

<div id="editPermissionModal" class="modal fade">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Ubah Permission</h4>
        <button type="button" class="btn btn-outline-danger close" data-bs-dismiss="modal"><span>&times;</span></button>
      </div>
      <div class="modal-body">
        <input type="hidden" name="idPermission" />
        <div class="mb-3 row">
          <label class="control-label col-sm-3 align-self-center mb-0" for="name">Nama Permission:</label>
          <div class="col-sm-9">
            <input type="text" class="form-control" id="editPermissionName" name="name" placeholder="Masukan Nama Permission Anda" value="">
          </div>
        </div>
        <div class="mb-3 row">
          <label class="control-label col-sm-3 align-self-center mb-0" for="username">Nama Guard :</label>
          <div class="col-sm-9">
            <input type="text" class="form-control" id="editGuardName" name="guard_name" placeholder="Masukan Guard" value="">
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <div class="btn-group">
          <button type="button" class="btn btn-danger btn-sm" data-bs-dismiss="modal">Cancel <i class="fa fa-fw fa-ban"></i></button>
          <button type="button" id="edit-permission" class="btn btn-success btn-sm">Simpan <i class="fa fa-fw fa-check"></i></button>
        </div>
      </div>
    </div>
  </div>
</div>

@endsection

@section('css')
<link rel="stylesheet" href="{{Module::asset('authentification:vendor/datatables/datatables.min.css')}}">
<link rel="stylesheet" href="{{Module::asset('authentification:vendor/select2/select2.min.css')}}">
<link rel="stylesheet" href="{{Module::asset('authentification:vendor/select2/select2-bootstrap-5-theme.min.css')}}">
@endsection

@section('scripts')
<script src="{{Module::asset('authentification:vendor/datatables/datatables.min.js')}}"></script>
<script src="{{Module::asset('authentification:vendor/select2/select2.full.min.js')}}"></script>
<script>
  $(document).ready(function() {
    let dt = $('#dt').DataTable({
      data: <?= isset($data->permissions) ? json_encode($data->permissions) : "[]"; ?>,
      info: false,
      paging: false,
      searching: false,
      ordering: false,
      columns: [{
          data: 'name',
          render: function(data, type, row, meta) {
            return `
                    <input type="hidden" name="permissions[${meta.row}][id]" value="${row.id}">
                    <input class="qty form-control form-control-sm text-right" name="permissions[${meta.row}][name]" value="${data}">`;
          }
        },
        {
          data: 'guard_name',
          render: function(data, type, row, meta) {
            return `<input class="qty form-control form-control-sm text-right" name="permissions[${meta.row}][guard_name]" value="${data}">`;
          }
        },
        {
          data: 'id',
          className: 'text-center',
          orderable: false,
          render: function(data, type, row, meta) {
            return `
                        <button type="button" class="btn btn-sm btn-secondary row-edit"><i class="fa-solid fa-pen-to-square"></i></button>
                        <button type="button" class="btn btn-sm btn-secondary row-remove"><i class="fa fa-minus"></i></button>
                        `;
          }
        }
      ],
      rowCallback: function(row, data, displayNum, displayIndex, dataIndex) {
        let api = this.api();

        $(row).find('.row-remove').click(function(e) {
          e.preventDefault();
          api.row(row).remove().draw();
        });

        $(row).find('.row-edit').click(function(e) {
          e.preventDefault();
          console.log(dataIndex);
          $('#editPermissionName').val(data.name);
          $('#editGuardName').val(data.guard_name);
          $('#idPermission').val(dataIndex);
          $('#editPermissionModal').modal('show');
        });
      },
    });

    $("#formStore").submit(function(e) {
      e.preventDefault();
      let form = $(this);
      let btnSubmit = form.find("[type='submit']");
      let btnSubmitHtml = btnSubmit.html();
      let url = form.attr("action");
      let data = new FormData(this);
      $.ajax({
        cache: false,
        processData: false,
        contentType: false,
        type: "POST",
        url: url,
        data: data,
        beforeSend: function() {
          btnSubmit.addClass("disabled").html('<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span> Loading...').prop("disabled", "disabled");
        },
        success: function(response) {
          let errorCreate = $('#errorCreate');
          errorCreate.css('display', 'none');
          errorCreate.find('.alert-text').html('');
          btnSubmit.removeClass("disabled").html(btnSubmitHtml).removeAttr("disabled");
          if (response.status === "success") {
            toastr.success(response.message, 'Success !');
            setTimeout(function() {
              if (response.redirect === "" || response.redirect === "reload") {
                location.reload();
              } else {
                location.href = response.redirect;
              }
            }, 1000);
          } else {
            toastr.error((response.message ? response.message : "Please complete your form"), 'Failed !');
            if (response.error !== undefined) {
              errorCreate.removeAttr('style');
              $.each(response.error, function(key, value) {
                errorCreate.find('.alert-text').append('<span style="display: block">' + value + '</span>');
              });
            }
          }
        },
        error: function(response) {
          btnSubmit.removeClass("disabled").html(btnSubmitHtml).removeAttr("disabled");
          toastr.error(response.responseJSON.message, 'Failed !');
        }
      });
    });

    $('#add-permission').click(function(e) {
      let permissionName = $('#permissionName').val(),
        guardName = $('#guardName').val();
      if (permissionName == '' || permissionName == undefined || permissionName == null || guardName == '' || guardName == undefined || guardName == null) {
        alert('Isi Form Terlebih Dahulu');
      } else {
        let data = {
          name: permissionName,
          guard_name: guardName,
        };
        dt.row.add(data).draw();
        $('#addPermissionModal').modal('hide');
        $('.modal-backdrop').remove();
      }
    });

    $('#addPermissionModal').on('hidden.bs.modal', function() {
      $('#permissionName').val(null);
      $('#guardName').val(null);
    });

    $('#editPermissionModal').on('hidden.bs.modal', function() {
      $('#editPermissionName').val(null);
      $('#editGuardName').val(null);
      $('#idPermission').val(null);
    });

    $('#edit-permission').click(function(e) {
      let permissionName = $('#editPermissionName').val(),
        guardName = $('#editGuardName').val(),
        idPermission = $('#idPermission').val();
      if (permissionName == '' || permissionName == undefined || permissionName == null || guardName == '' || guardName == undefined || guardName == null) {
        alert('Isi Form Terlebih Dahulu');
      } else {
        let data = {
          name: permissionName,
          guard_name: guardName,
        };
        dt.row(idPermission).data(data).draw();
        $('#editPermissionModal').modal('hide');
        $('.modal-backdrop').remove();
      }
    });
  });
</script>
@endsection
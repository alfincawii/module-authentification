@extends('authentification::layouts.main')
@section('contents')
<form id="formStore" action="{{ $config['form']->action }}" method="POST">
  @method($config['form']->method)
  @csrf
  <div class="row">
    <div class="col-md-12">
      <div class="card">
        <div class="card-header">
          <div class="row">
            <div class="col-md-6 col-12 d-flex justify-content-center justify-content-md-start align-items-center mb-3 mb-md-0">
              <h3 class="card-title">Tambah Role</h3>
            </div>
            <div class="col-md-6 col-12 d-flex justify-content-center justify-content-md-end align-items-baseline mb-md-0 mb-2">
              <div class="btn-group float-md-right" role="group" aria-label="Button group with nested dropdown">
                <a onclick="history.back()" class="btn btn-outline-primary float-end">
                  <i class="fa-solid fa-rotate-left"></i> Kembali
                </a>
                <button type="submit" class="btn btn-success text-white float-end">
                  <i class="fa-solid fa-plus"></i> Simpan
                </button>
              </div>
            </div>
          </div>
        </div>
        <div class="form-group">
          <div id="errorCreate" class="mb-3" style="display:none;">
            <div class="alert alert-danger" role="alert">
              <div class="alert-icon"><i class="flaticon-danger text-danger"></i></div>
              <div class="alert-text">
              </div>
            </div>
          </div>
          <label class="form-label" for="name">Nama :</label>
          <input type="text" class="form-control" value="{{ $data->name ?? '' }}" name="name" id="name">
        </div>
      </div>
      <div class="card">
        <div class="card-header">
          <div class="row">
            <div class="col-md-6 col-12 d-flex justify-content-center justify-content-md-start align-items-center mb-3 mb-md-0">
              <h3 class="card-title">Module Permissions</h3>
            </div>
          </div>
        </div>
        <div class="accordion" id="accordionPanelsStayOpenExample">
          @foreach ($module as $item )
          <div class="accordion-item">
            <h2 class="accordion-header collapsed" id="heading-{{$item->id}}">
              <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#collapse-{{$item->id}}" aria-expanded="false" aria-controls="collapse-{{$item->id}}">
                {{$item->name}}
              </button>
            </h2>
            <div id="collapse-{{$item->id}}" class="accordion-collapse collapse" aria-labelledby="heading-{{$item->id}}">
              <div class="accordion-body">
                @php
                $col = 0 ;
                $role = \Spatie\Permission\Models\Role::find($data->id);
                @endphp
                @foreach ($item->permissions as $permission )
                @if ($col == 0)
                <div class="row">
                  @endif
                  <div class="col-md-3">
                    <div class="form-check form-check-inline">
                      <input class="form-check-input" type="checkbox" name="permissions[]" id="permission{{ $permission->id }}" value="{{ $permission->name }}" {{ isset($role) && $role->hasPermissionTo($permission->name) ? 'checked' : '' }}>
                      <label class="form-check-label" for="permission{{ $permission->id }}">{{ $permission->name }}</label>
                    </div>
                  </div>
                  @if ($col == 5)
                  @php
                  $col = 0;
                  @endphp
                </div>
                @else
                @php
                $col++;
                @endphp
                @endif
                @endforeach
              </div>
            </div>
          </div>
          @endforeach
        </div>
      </div>
    </div>
  </div>
</form>


@endsection

@section('css')
<link rel="stylesheet" href="{{Module::asset('authentification:vendor/datatables/datatables.min.css')}}">
@endsection

@section('scripts')
<script src="{{Module::asset('authentification:vendor/datatables/datatables.min.js')}}"></script>
<script>
  $(document).ready(function() {
    $('.collapse').collapse
    $("#formStore").submit(function(e) {
      e.preventDefault();
      let form = $(this);
      let btnSubmit = form.find("[type='submit']");
      let btnSubmitHtml = btnSubmit.html();
      let url = form.attr("action");
      let data = new FormData(this);
      $.ajax({
        cache: false,
        processData: false,
        contentType: false,
        type: "POST",
        url: url,
        data: data,
        beforeSend: function() {
          btnSubmit.addClass("disabled").html('<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span> Loading...').prop("disabled", "disabled");
        },
        success: function(response) {
          let errorCreate = $('#errorCreate');
          errorCreate.css('display', 'none');
          errorCreate.find('.alert-text').html('');
          btnSubmit.removeClass("disabled").html(btnSubmitHtml).removeAttr("disabled");
          if (response.status === "success") {
            toastr.success(response.message, 'Success !');
            setTimeout(function() {
              if (response.redirect === "" || response.redirect === "reload") {
                location.reload();
              } else {
                location.href = response.redirect;
              }
            }, 1000);
          } else {
            toastr.error((response.message ? response.message : "Please complete your form"), 'Failed !');
            if (response.error !== undefined) {
              errorCreate.removeAttr('style');
              $.each(response.error, function(key, value) {
                errorCreate.find('.alert-text').append('<span style="display: block">' + value + '</span>');
              });
            }
          }
        },
        error: function(response) {
          btnSubmit.removeClass("disabled").html(btnSubmitHtml).removeAttr("disabled");
          toastr.error(response.responseJSON.message, 'Failed !');
        }
      });
    });
  });
</script>
@endsection
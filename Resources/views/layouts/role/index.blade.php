@extends('authentification::layouts.main')
@section('contents')
<div class="card">
  <div class="card-header">
    <div class="row">
      <div class="col-md-6 col-12 d-flex justify-content-center justify-content-md-start align-items-center mb-3 mb-md-0">
        <h3 class="card-title">Daftar Role</h3>
      </div>
      <div class="col-md-6 col-12 d-flex justify-content-center justify-content-md-end align-items-baseline mb-md-0 mb-2">
        <div class="btn-group float-md-right" role="group" aria-label="Button group with nested dropdown">
          <a class="btn btn-outline-primary" href=""><i class="fa fa-sync-alt fa-fw"></i></a>
          <a class="btn btn-outline-primary" href="{{ route('roles.create') }}"><i class="fa fa-plus fa-fw"></i> Tambah</a>
        </div>
      </div>
    </div>
  </div>
  <div class="table-responsive">
    <table id="dt" class="table table-bordered w-100">
      <thead>
        <tr>
          <th>Nama</th>
          <th class="text-center">Aksi</th>
        </tr>
      </thead>
      <tbody></tbody>
    </table>
  </div>
</div>

<div id="confirmDeleteModal" class="modal fade">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Delete Item Confirm</h4>
        <button type="button" class="btn btn-outline-danger close" data-bs-dismiss="modal"><span>&times;</span></button>
      </div>
      <div class="modal-body">
        <input type="hidden" name="id" />
        <div class="alert alert-warning">
          <strong>Peringatan !</strong> Anda yakin menghapus data ini ?
        </div>
      </div>
      <div class="modal-footer">
        <div class="btn-group">
          <button type="button" class="btn btn-danger btn-sm" data-bs-dismiss="modal">No <i class="fa fa-fw fa-ban"></i></button>
          <button type="button" id="btnDelete" class="btn btn-success btn-sm">Yes <i class="fa fa-fw fa-check"></i></button>
        </div>
      </div>
    </div>
  </div>
</div>

@endsection

@section('css')
<link rel="stylesheet" href="{{Module::asset('authentification:vendor/datatables/datatables.min.css')}}">
@endsection

@section('scripts')
<script src="{{Module::asset('authentification:vendor/datatables/datatables.min.js')}}"></script>
<script>
  $(document).ready(function() {
    $('#dt').dataTable({
      serverSide: true,
      processing: true,
      ajax: {
        url: `{{ route('roles.index') }}`
      },
      columns: [{
          data: 'name',
          name: 'name'
        },
        {
          data: 'action',
          name: 'action',
          className: "text-center",
          orderable: false,
          searchable: false
        },
      ],
      rowCallback: function(row, data) {
        let api = this.api();
        $(row).find('.btn-delete').click(function() {
          var pk = $(this).data('id'),
            url = `{{ route("roles.index") }}/` + pk;
          console.log(pk);

          $('#confirmDeleteModal').modal('toggle');

          $('#btnDelete').click(function(e) {
            e.preventDefault();
            $.ajax({
              url: url,
              type: "DELETE",
              data: {
                _token: '{{ csrf_token() }}',
                _method: 'DELETE'
              },
              error: function(response) {
                toastr.error(response, 'Failed !');
              },
              success: function(response) {
                if (response.status === "success") {
                  toastr.success(response.message, 'Success !');
                  $('#confirmDeleteModal').modal('hide');;
                  $('.modal-backdrop').remove();
                  api.draw();
                } else {
                  toastr.error((response.message ? response.message : "Please complete your form"), 'Failed !');
                }
              }
            });
          });
        });
      }
    });

  });
</script>
@endsection
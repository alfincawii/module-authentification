<div class="row breadcrumbs-top">
  <div class="breadcrumb-wrapper col-12">
    <ol class="breadcrumb">
      <li class="breadcrumb-item"><a href="{{ url('/') }}"><i class="fa fa-home"></i></a></li>
      @if(isset($config['breadcrumbs']) && is_array($config['breadcrumbs']) && count($config['breadcrumbs']) > 0)
      @foreach ($config['breadcrumbs'] as $item)
      @php $item = (object)$item @endphp
      <li class="breadcrumb-item" aria-current="page">
        @if (isset($item->href))
        <a href="{{ url($item->href) }}">{{ ucwords($item->label) }}</a>
        @else
        {{ ucwords($item->label) }}
        @endif
      </li>
      @endforeach
      @endif
    </ol>
  </div>
</div>
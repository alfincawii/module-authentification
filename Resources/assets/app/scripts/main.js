"use strict";
$(window).on("load", function (e) {
  $(".preloader-wrapper").hide();
}),
  $(document).ready(function () {
    $(".menu-opener").click(function () {
      $(".menu-opener, .menu-opener-inner, .side-menu").toggleClass("active");
    }),
      $(".mobile-icon-right").click(function () {
        $("header > .main-header > .right").toggleClass("active");
      }),
      $(".side-menu").find("ul > li").has("ul").addClass("child"),
      $(".side-menu").find("ul > .child > a").removeAttr("href"),
      $(".side-menu").find("ul > li > ul").has(".active").addClass("active"),
      $(".side-menu").find("ul > li").has(".active").addClass("open"),
      $(".side-menu")
        .find("ul > li")
        .has("ul")
        .find("a")
        .click(function () {
          $(this).next().toggleClass("active"),
            $(this).parent(".child").toggleClass("open");
        }),
      $("#btn-hide-aside").click(function () {
        $(
          ".side-menu, .main-header > .left, .main-header > .right, .body-admin > .body"
        ).toggleClass("wrapping");
      });
  });

<?php

namespace Modules\Authentification\Classes;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Str;
use Modules\Authentification\Entities\MenuManager;

class Menu
{

  public static function sidebar()
  {
    $menuManager = new MenuManager();
    $roleId = isset(Auth::user()->role_id) ? Auth::user()->role_id : NULL;
    $menu_list = $menuManager->with('module')->get();
    $roots = [];
    foreach ($menu_list as $i => $v) :
      if ($v->parent_id == 0) {
        array_push($roots, $v->id);
      } else {
        array_push($roots, $v->parent_id);
      }
    endforeach;
    $roots = array_unique($roots);
    $roots = MenuManager::whereIn('id', $roots)->orderBy('sort', 'asc')->get();
    return self::tree($roots, $menu_list, $roleId);
  }

  public static function tree($roots, $menu_list, $roleId, $parentId = 0, $endChild = 0)
  {
    $html = '';
    foreach ($roots as $i => $v) :
      if ($v->type == 'module') {
        $html .= '<li class="' . ((request()->is(substr($v->module->href . '*', 1))) ? 'active' : '') . '">
                     <a href="' . $v->module->href . '">
                        <i class="' . ($v->icon ?? '') . '"></i>
                        ' . $v->title . '
                     </a></li>
               ';
      } else if ($v->type == 'header') {
        $html .= '<li class="list-title">' . $v->title . '</li>';
      } else {
        $html .= '<li>
                     <a  href="" >
                        <i class="' . ($v->icon ?? '') . '"></i>' . $v->title . '
                     </a>
                     <ul class="">
               ';

        $list_menu = $menu_list->where('parent_id', $v->id)->sortBy('sort');
        foreach ($list_menu as $index => $item) :
          $html .= '
                    <li class="' . ((request()->is(substr($item->module->href . '*', 1))) ? 'active' : '') . '">
                            <a href="' . URL::to($item->module->href) . '">' . $item->title . '
                            </a>
                        </li>
                  ';
        endforeach;
        $html .= '</ul></li>';
      }
    endforeach;
    return $html;
  }
}

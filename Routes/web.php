<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Modules\Authentification\Http\Controllers\AuthentificationController;
use Modules\Authentification\Http\Controllers\DashboardController;
use Modules\Authentification\Http\Controllers\MenuController;
use Modules\Authentification\Http\Controllers\MenuManagerController;
use Modules\Authentification\Http\Controllers\RoleController;
use Modules\Authentification\Http\Controllers\SearchController;
use Modules\Authentification\Http\Controllers\UserController;


// Auth Route
Route::get('login', [AuthentificationController::class, 'showLogin'])->name('login');
Route::post('login', [AuthentificationController::class, 'login'])->name('login.post');
Route::post('logout', [AuthentificationController::class, 'logout'])->name('logout');


Route::middleware('auth')->group(function () {
    // Search Route
    Route::get('search/roles', [SearchController::class, 'roles'])->name('auth.searchRole');
    Route::get('search/module', [SearchController::class, 'modules'])->name('auth.searchModule');

    // Dashboard Route
    Route::resource('home', DashboardController::class);

    //  Users Route
    Route::resource('users', UserController::class);

    // Roles Route
    Route::resource('roles', RoleController::class);

    // Menu Route
    Route::resource('module', MenuController::class);

    // Menu Manager Route
    Route::post('menu-manager/changeHierarchy', [MenuManagerController::class, 'changeHierarchy'])->name('menu-manager.changeHierarchy');
    Route::resource('menu-manager', MenuManagerController::class);
});

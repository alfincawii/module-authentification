<?php

namespace Modules\Authentification\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Modules\Authentification\Entities\MenuManager;
use Modules\Authentification\Traits\ResponseStatus;

class MenuManagerController extends Controller
{
  use ResponseStatus;


  public function __construct()
  {
    $this->middleware('permission:menu-manager-index', ['only' => ['index']]);
    $this->middleware('permission:menu-manager-create', ['only' => ['create', 'store']]);
    $this->middleware('permission:menu-manager-edit', ['only' => ['edit', 'update']]);
    $this->middleware('permission:menu-manager-delete', ['only' => ['destroy']]);
  }
  /**
   * Display a listing of the resource.
   * @return Renderable
   */
  public function index()
  {
    $config = [
      'title' => 'Menu Manager'
    ];
    $config['breadcrumbs'] = [
      ['label' => 'Menu Manager']
    ];
    $config['form'] = (object)[
      'method' => 'POST',
      'action' => route('menu-manager.store')
    ];

    $sortable = self::getMenu();
    return view('authentification::layouts.menu-manager.index', compact('config', 'sortable'));
  }

  /**
   * Show the form for creating a new resource.
   * @return Renderable
   */
  public function create()
  {
    return view('authentification::create');
  }

  /**
   * Store a newly created resource in storage.
   * @param Request $request
   * @return Renderable
   */
  public function store(Request $request)
  {

    $validator = Validator::make($request->all(), [
      'type' => 'required|in:module,header,line,static',
    ]);
    if ($validator->passes()) {
      DB::beginTransaction();
      try {
        $menuManager = MenuManager::create([
          'title' => ucwords($request['title']),
          'icon' => $request['icon'],
          'type' => $request['type'],
          'sort' => MenuManager::where([
            ['parent_id', $request['parent_id'] ?? 0]
          ])->max('sort') + 1,
          'module_id' => $request['module_id']
        ]);

        DB::commit();
        $response = response()->json($this->responseStore(true, NULL, route('menu-manager.index')));
      } catch (Throwable $throw) {
        DB::rollBack();
        $response = $throw;
      }
    } else {
      $response = response()->json(['error' => $validator->errors()->all()]);
    }
    return $response;
  }

  /**
   * Show the specified resource.
   * @param int $id
   * @return Renderable
   */
  public function show($id)
  {
    return view('authentification::show');
  }

  /**
   * Show the form for editing the specified resource.
   * @param int $id
   * @return Renderable
   */
  public function edit($id)
  {
    $config['title'] = "Menu Manager";
    $config['breadcrumbs'] = [
      ['label' => 'Menu Manager']
    ];
    $config['form'] = (object)[
      'method' => 'PUT',
      'action' => route('menu-manager.update', $id)
    ];
    $data = MenuManager::find($id)->with('module')->where('id', $id)->first();

    $sortable = self::getMenu();
    return view('authentification::layouts.menu-manager.index', compact('config', 'sortable', 'data'));
  }

  /**
   * Update the specified resource in storage.
   * @param Request $request
   * @param int $id
   * @return Renderable
   */
  public function update(Request $request, $id)
  {

    $validator = Validator::make($request->all(), [
      'type' => 'required|in:module,header,line,static'
    ]);

    if ($validator->passes()) {
      $menuManager = MenuManager::find($id);
      DB::beginTransaction();
      try {
        if ($request['type'] == 'module') {
          $menuManager->update([
            'title' => $request['title'],
            'type' => $request['type'],
            'icon' => $request['icon'],
          ]);
        } elseif ($request['type'] == 'header' || $request['type'] == 'static') {
          $menuManager->update([
            'title' => $request['title'],
            'type' => $request['type'],
            'icon' => $request['icon'],
            'module_id' => NULL
          ]);
        } elseif ($request['type'] == 'line') {
          if ($menuManager->type == 'module' && $permission->count() > 0) {
            $permission->delete();
          }
          $menuManager->update([
            'title' => NULL,
            'type' => $request['type'],
            'icon' => NULL,
            'module_id' => NULL
          ]);
        }

        DB::commit();
        $response = response()->json($this->responseStore(true, NULL, route('menu-manager.index')));
      } catch (\Exception $e) {
        DB::rollback();
        $response = response()->json([
          'status' => 'error',
          'message' => $e
        ]);
      }
    } else {
      $response = response()->json(['error' => $validator->errors()->all()]);
    }
    return $response;
  }

  /**
   * Remove the specified resource from storage.
   * @param int $id
   * @return Renderable
   */
  public function destroy($id)
  {
    $response = response()->json([
      'status' => 'error',
      'message' => 'Data gagal dihapus'
    ]);
    DB::beginTransaction();

    try {
      $data = MenuManager::findOrFail($id);
      $data->delete();
      DB::commit();
      $response = response()->json([
        'status' => 'success',
        'message' => 'Data berhasil dihapus'
      ]);
    } catch (Throwable $throw) {
      dd($throw);
      $response = response()->json(['error' => $throw]);
    }
    return $response;
  }

  public static function getMenu()
  {
    $menuManager = new MenuManager;
    $menu_list = $menuManager->getall();
    $roots = $menu_list->where('parent_id', 0);
    return self::tree($roots, $menu_list);
  }

  private static function tree($roots, $menu_list)
  {
    $html = '<ol class="dd-list"> ';
    foreach ($roots as $item) {
      $find = $menu_list->where('parent_id', $item['id']);
      $html .= '
                     <li class="dd-item dd3-item" data-id="' . $item->id . '">
                       <div class="dd-handle dd3-handle"></div>
                       <div class="dd3-content">' . ($item->type == 'line' ? 'Line' : $item->title) . '</div>
                       <div class="dd3-actions">
                         <div class="btn-group">';
      if ($item->type == 'module') {
        $html .= '<a href="#" class="btn btn-sm font-size-14">M</a>';
      } else if ($item->type == 'header') {
        $html .= '<a href="#" class="btn btn-sm font-size-14">H</a>';
      } else if ($item->type == 'line') {
        $html .= '<a href="#" class="btn btn-sm font-size-14">L</a>';
      } else if ($item->type == 'static') {
        $html .= '<a href="#" class="btn btn-sm font-size-14">S</a>';
      }
      $html .= '<a href="' . route('menu-manager.edit', $item->id) . '" class="btn btn-sm btn-default"><i class="fa fa-fw fa-edit"></i></a>
                        <button
                          type="button"
                          class="btn btn-sm btn-delete btn-default"
                          data-id="' . $item->id . '"
                          ><i class="fa fa-fw fa-trash"></i>
                        </button>
                      </div>
                    </div>
                  ';

      if ($find->count()) {
        $html .= self::tree($find, $menu_list);
      }
      $html .= '</li>';
    }
    $html .= '</ol>';
    return $html;
  }

  public function changeHierarchy(Request $request)
  {
    $data = json_decode($request['hierarchy'], TRUE);
    $menuItems = $this->render_menu_hierarchy($data);

    DB::beginTransaction();
    try {
      foreach ($menuItems as $item) :
        MenuManager::find($item['id'])->update([
          'parent_id' => $item['parent_id'],
          'sort' => $item['sort']
        ]);
      endforeach;
      DB::commit();
      $response = response()->json([
        'status' => 'success',
        'message' => 'Data berhasil dihapus',
        'redirect' => "reload"
      ]);
    } catch (\Exception $e) {
      DB::rollback();
      $response = response()->json([
        'status' => 'error',
        'message' => 'Gagal menghapus data'
      ]);
    }
    return $response;
  }

  public function render_menu_hierarchy($data = array(), $parentMenu = 0, $result = array())
  {
    foreach ($data as $key => $val) {
      $row['id'] = $val['id'];
      $row['parent_id'] = $parentMenu;
      $row['sort'] = ($key + 1);
      array_push($result, $row);
      if (isset($val['children']) && $val['children'] > 0) {
        $result = array_merge($result, $this->render_menu_hierarchy($val['children'], $val['id']));
      }
    }
    return $result;
  }
}

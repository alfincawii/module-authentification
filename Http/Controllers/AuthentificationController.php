<?php

namespace Modules\Authentification\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use Modules\Authentification\Traits\RedirectsUsers;
use Modules\Authentification\Traits\ResponseStatus;
use Modules\Authentification\Traits\ThrottlesLogins;
use Modules\Authentification\Traits\ValidateLogin;

class AuthentificationController extends Controller
{
    use ValidateLogin, RedirectsUsers, ThrottlesLogins, ResponseStatus;

    protected $redirectTo = '/home';

    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function showLogin()
    {
        return view('authentification::layouts.auth.login');
    }

    public function login(Request $request)
    {
        $this->validateLogin($request);

        if (
            method_exists($this, 'hasTooManyLoginAttempts') &&
            $this->hasTooManyLoginAttempts($request)
        ) {
            $this->fireLockoutEvent($request);
            $this->sendLockoutResponse($request);
        }

        $remember = $request->has('remember');
        $data = $request->only('email', 'password');
        $fieldType = filter_var($request->email, FILTER_VALIDATE_EMAIL) ? 'email' : 'username';
        try {
            if (Auth::guard()->attempt(array($fieldType => $request['email'], 'password' => $request['password']), $remember)) {
                return response()->json($this->responseLogin(true, NULL, route('home.index')));
            } else {
                $this->incrementLoginAttempts($request);
                $this->sendFailedLoginResponse($request);
                return redirect()->route('login');
            }
        } catch (Throwable $e) {
            $response = response()->json([
                'error' => 'Email atau password salah',
                'message' => 'Email atau password salah'
            ]);
        }
    }


    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function logout(Request $request)
    {
        $this->guard()->logout();
        $request->session()->invalidate();
        $request->session()->regenerate();
        $redirect = redirect()->route('login');
        return $redirect;
    }
}

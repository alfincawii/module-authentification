<?php

namespace Modules\Authentification\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Authentification\Entities\Menu;
use Spatie\Permission\Models\Role;

class SearchController extends Controller
{
    public function roles(Request $request)
    {
        $response = Role::where([['guard_name', '=', 'root']])->select('*');
        if ($request->has('q')) {
            $response = $response->where('name', 'LIKE', '%' . $request->q . '%');
        } elseif ($request->has('term')) {
            $response = $response->where('name', 'LIKE', '%' . $request->term . '%');
        }
        $response = $response->paginate(25);
        return response()->json($response);
    }

    public function modules(Request $request)
    {
        $response = Menu::select('*');
        if ($request->has('q')) {
            $response = $response->where('name', 'LIKE', '%' . $request->q . '%');
        } elseif ($request->has('term')) {
            $response = $response->where('name', 'LIKE', '%' . $request->term . '%');
        }
        $response = $response->paginate(25);
        return response()->json($response);
    }
}

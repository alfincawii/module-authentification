<?php

namespace Modules\Authentification\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Nwidart\Modules\Facades\Module;
use DataTables;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Modules\Authentification\Traits\ResponseStatus;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Str;
use Modules\Authentification\Entities\Menu;

class RoleController extends Controller
{
    use ResponseStatus;
    public function __construct()
    {
        $this->middleware('permission:role-index', ['only' => ['index']]);
        $this->middleware('permission:role-create', ['only' => ['create', 'store']]);
        $this->middleware('permission:role-edit', ['only' => ['edit', 'update']]);
        $this->middleware('permission:role-delete', ['only' => ['destroy']]);
    }
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index(Request $request)
    {
        $config = [
            'title' => 'Roles'
        ];
        $config['breadcrumbs'] = [
            ['label' => 'List Roles']
        ];
        if ($request->ajax()) {
            $data = Role::all();
            return DataTables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function ($row) {
                    $actionBtn = '<div class="btn-group dropend">
                           <button type="button" class="btn btn-outline-info dropdown-toggle" data-bs-toggle="dropdown" aria-expanded="false">
                               Aksi
                           </button>
                           <ul class="dropdown-menu">
                               <li><a class="dropdown-item" href="' . route('roles.edit', $row->id) . '">Edit</a></li>
                               <li><a class="dropdown-item btn-delete" data-bs-toggle="modal" data-bs-target="#confirmDeleteModal" data-id ="' . $row->id . '" >Hapus</a></li>
                           </ul>
                         </div>';
                    return $actionBtn;
                })->make(true);
        }

        // dd(module_path('Authentification'));
        return view('authentification::layouts.role.index', compact('config'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create()
    {
        $config = [
            'title' => 'Tambah Role'
        ];

        $config['form'] = (object)[
            'method' => 'POST',
            'action' => route('roles.store')
        ];
        $config['breadcrumbs'] = [
            ['label' => 'List Roles', 'href' => route('roles.index')],
            ['label' => 'Tambah Role']
        ];

        $module = Menu::with('permissions')->get();

        return view('authentification::layouts.role.form', compact('config', 'module'));
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name'          => 'required|unique:roles',
        ]);

        if ($validator->passes()) {
            DB::beginTransaction();
            try {

                $role = new Role;
                $role->guard_name   = 'web';
                $role->name         = $request['name'];
                $role->slug         = Str::slug($request['name']);
                $role->save();


                DB::commit();
                $response = Response()->json($this->responseStore(true, NULL, route('roles.index')));
            } catch (Throwable $throw) {
                dd($throw);
                DB::rollBack();
                $response = response()->json(['error' => $throw]);
            }
        } else {
            $response = response()->json(['error' => $validator->errors()->all()]);
        }
        return $response;
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show($id)
    {
        return view('authentification::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit($id)
    {
        $config['title'] = "Edit Role";

        $data = Role::find($id);


        $config['form'] = (object)[
            'method' => 'PUT',
            'action' => route('roles.update', $id)
        ];
        $config['breadcrumbs'] = [
            ['label' => 'List Roles', 'href' => route('roles.index')],
            ['label' => 'Edit Role']
        ];
        $module = Menu::with('permissions')->get();

        return view('authentification::layouts.role.form', compact('config', 'data', 'module'));
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|unique:roles,name,' . $request['name'] . ',name',
        ]);

        if ($validator->passes()) {
            DB::beginTransaction();
            try {
                $role = Role::findorfail($id);
                $role->guard_name   = 'web';
                $role->name         = $request['name'];
                $role->slug = Str::slug($request['name']);
                $role->save();

                $role->syncPermissions($request->permissions);

                DB::commit();
                $response = response()->json($this->responseStore(true, NULL, route('roles.index')));
            } catch (Throwable $throw) {
                dd($throw);
                DB::rollBack();
                $response = response()->json(['error' => $throw]);
            }
        } else {
            $response = response()->json(['error' => $validator->errors()->all()]);
        }
        return $response;
    }
    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy(Role $role)
    {

        $response = response()->json([
            'status' => 'error',
            'message' => 'Data gagal dihapus'
        ]);
        DB::beginTransaction();

        try {
            $role->delete();
            $response = response()->json([
                'status' => 'success',
                'message' => 'Data berhasil dihapus'
            ]);
            DB::commit();
        } catch (Throwable $throw) {
            dd($throw);
            $response = response()->json(['error' => $throw]);
        }

        return $response;
    }
}

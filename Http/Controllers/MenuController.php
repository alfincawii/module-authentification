<?php

namespace Modules\Authentification\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Authentification\Entities\Menu;
use DataTables;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Modules\Authentification\Traits\ResponseStatus;
use Spatie\Permission\Models\Permission;

class MenuController extends Controller
{
    use ResponseStatus;
    public function __construct()
    {
        $this->middleware('permission:modules-index', ['only' => ['index']]);
        $this->middleware('permission:modules-create', ['only' => ['create', 'store']]);
        $this->middleware('permission:modules-edit', ['only' => ['edit', 'update']]);
        $this->middleware('permission:modules-delete', ['only' => ['destroy']]);
    }
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index(Request $request)
    {
        $config = [
            'title' => 'Module'
        ];
        $config['breadcrumbs'] = [
            ['label' => 'List Modules']
        ];


        if ($request->ajax()) {
            $data = Menu::latest();
            return DataTables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function ($row) {
                    $actionBtn = '<div class="btn-group dropend">
                       <button type="button" class="btn btn-outline-info dropdown-toggle" data-bs-toggle="dropdown" aria-expanded="false">
                           Aksi
                       </button>
                       <ul class="dropdown-menu">
                           <li><a class="dropdown-item" href="' . route('module.edit', $row->id) . '">Edit</a></li>
                           <li><a class="dropdown-item btn-delete" data-bs-toggle="modal" data-bs-target="#confirmDeleteModal" data-id ="' . $row->id . '" >Hapus</a></li>
                       </ul>
                     </div>';
                    return $actionBtn;
                })->make(true);
        }

        // dd(module_path('Authentification'));
        return view('authentification::layouts.menus.index', compact('config'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create()
    {
        $config = [
            'title' => 'Tambah Module'
        ];

        $config['form'] = (object)[
            'method' => 'POST',
            'action' => route('module.store')
        ];
        $config['breadcrumbs'] = [
            ['label' => 'List Module', 'href' => route('module.index')],
            ['label' => 'Tambah Module']
        ];

        return view('authentification::layouts.menus.form', compact('config'));
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'permissions'        => 'required|array',
            'name'              => 'required|unique:menus',
            'href'              => 'required',
            'slug'              => 'required|unique:menus',
        ]);
        if ($validator->passes()) {
            DB::beginTransaction();
            try {
                $data = new Menu;
                $data->name = $request->name;
                $data->href = $request->href;
                $data->slug = $request->slug;
                $data->save();

                $data->permissions()->createMany($request->permissions);


                DB::commit();
                $response = response()->json($this->responseStore(true, NULL, route('module.index')));
            } catch (\Throwable $throw) {
                dd($throw);
                DB::rollBack();
                $response = response()->json($this->responseStore(false));
            }
        } else {
            $response = response()->json(['error' => $validator->errors()->all()]);
        }
        return $response;
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show($id)
    {
        return view('authentification::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit($id)
    {
        $config = [
            'title' => 'Edit Module'
        ];

        $config['form'] = (object)[
            'method' => 'PUT',
            'action' => route('module.update', $id)
        ];

        $data = Menu::with('permissions')->where('id', $id)->first();

        $config['breadcrumbs'] = [
            ['label' => 'List Module', 'href' => route('module.index')],
            ['label' => 'Edit Module']
        ];

        return view('authentification::layouts.menus.form', compact('config', 'data'));
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(Request $request, $id)
    {
        $rules = [
            'permissions'               => 'required|array',
            'name'                      => 'required|unique:menus,name,' . $request['name'] . ',name',
            'href'                      => 'required',
            'slug'                      => 'required|unique:menus,slug,' . $request['slug'] . ',slug',
        ];

        foreach ($request->permissions as $key => $permission) {
            $rules = array_merge($rules, ['permissions.' . $key . '.name' => 'required|unique:permissions,name,' . $permission['name'] . ',name']);
        }

        $validator = Validator::make($request->all(), $rules);
        if ($validator->passes()) {
            DB::beginTransaction();
            try {
                $data = Menu::find($id);
                $data->name = $request->name;
                $data->href = $request->href;
                $data->slug = $request->slug;
                $data->save();

                $permissions = array_map(function ($item) use ($data) {
                    return [
                        'name'          => $item['name'],
                        'guard_name'    => $item['guard_name'],
                        'menu_id'       => $data->id
                    ];
                }, (isset($request['permissions']) ? $request['permissions'] : []));

                Permission::upsert(
                    $permissions,
                    ['name', 'guard_name', 'menu_id'],
                    ['name', 'guard_name', 'menu_id']
                );

                $usedPermission = [];

                foreach ($permissions as $key => $value) {
                    array_push($usedPermission, $value['name']);
                }


                $permission = Permission::where('menu_id', $data->id)->whereNotIn('name', $usedPermission)->delete();

                Artisan::call('cache:forget spatie.permission.cache ');

                DB::commit();
                $response = response()->json($this->responseStore(true, NULL, route('module.index')));
            } catch (\Throwable $throw) {
                dd($throw);
                DB::rollBack();
                $response = response()->json($this->responseStore(false));
            }
        } else {
            $response = response()->json(['error' => $validator->errors()->all()]);
        }
        return $response;
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy($id)
    {

        $response = response()->json([
            'status' => 'error',
            'message' => 'Data gagal dihapus'
        ]);
        DB::beginTransaction();

        try {
            $menu = Menu::find($id);
            $menu->permissions()->delete();
            $menu->delete();
            $response = response()->json([
                'status' => 'success',
                'message' => 'Data berhasil dihapus'
            ]);
            DB::commit();
        } catch (Throwable $throw) {
            dd($throw);
            $response = response()->json(['error' => $throw]);
        }

        return $response;
    }
}

<?php

namespace Modules\Authentification\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use DataTables;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Modules\Authentification\Entities\User;
use Modules\Authentification\Traits\ResponseStatus;
use Modules\Authentification\Helpers\ImageUpload;

class UserController extends Controller
{
    use ResponseStatus;


    public function __construct()
    {
        $this->middleware('permission:users-index', ['only' => ['index']]);
        $this->middleware('permission:users-create', ['only' => ['create', 'store']]);
        $this->middleware('permission:users-edit', ['only' => ['edit', 'update']]);
        $this->middleware('permission:users-delete', ['only' => ['destroy']]);
    }
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index(Request $request)
    {
        $config = [
            'title' => 'Pengguna'
        ];
        $config['breadcrumbs'] = [
            ['label' => 'List Pengguna']
        ];


        if ($request->ajax()) {
            $data = User::with('roles');
            return DataTables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function ($row) {
                    $actionBtn = '<div class="btn-group dropend">
                       <button type="button" class="btn btn-outline-info dropdown-toggle" data-bs-toggle="dropdown" aria-expanded="false">
                           Aksi
                       </button>
                       <ul class="dropdown-menu">
                           <li><a class="dropdown-item" href="' . route('users.edit', $row->id) . '">Edit</a></li>
                           <li><a class="dropdown-item btn-delete" data-bs-toggle="modal" data-bs-target="#confirmDeleteModal" data-id ="' . $row->id . '" >Hapus</a></li>
                       </ul>
                     </div>';
                    return $actionBtn;
                })->make(true);
        }

        // dd(module_path('Authentification'));
        return view('authentification::layouts.user.index', compact('config'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create()
    {
        $config = [
            'title' => 'Tambah Pengguna'
        ];

        $config['form'] = (object)[
            'method' => 'POST',
            'action' => route('users.store')
        ];
        $config['breadcrumbs'] = [
            ['label' => 'List Pengguna', 'href' => route('users.index')],
            ['label' => 'Tambah Pengguna']
        ];

        return view('authentification::layouts.user.form', compact('config'));
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'roles'         => 'required|array',
            'name'          => 'required',
            'username'      => 'required|alpha_dash|unique:users',
            'password'      => 'required|between:6,255|confirmed',
            'email'         => 'required|unique:users,email|email',
            'active'        => 'required|between:0,1',
            'image'         => 'nullable|image|mimes:jpeg,png,jpg,gif,svg|max:2024',
        ]);
        if ($validator->passes()) {
            DB::beginTransaction();
            try {
                $data = new User();
                $data->name = $request->name;
                $data->password = Hash::make($request->password);
                $data->username = $request->username;
                $data->email = $request->email;
                $data->active = $request->active;
                $data->image = ImageUpload::profilePicture('image', 'public');
                $data->save();

                $data->syncRoles($request['roles']);

                DB::commit();
                $response = response()->json($this->responseStore(true, NULL, route('users.index')));
            } catch (\Throwable $throw) {
                dd($throw);
                DB::rollBack();
                $response = response()->json($this->responseStore(false));
            }
        } else {
            $response = response()->json(['error' => $validator->errors()->all()]);
        }
        return $response;
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show($id)
    {
        return view('authentification::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit($id)
    {
        $config = [
            'title' => 'Edit Pengguna'
        ];

        $data = User::with('roles')->where('id', $id)->first();

        $config['form'] = (object)[
            'method' => 'PUT',
            'action' => route('users.update', $id)
        ];
        $config['breadcrumbs'] = [
            ['label' => 'List Pengguna', 'href' => route('users.index')],
            ['label' => 'Edit Pengguna']
        ];

        return view('authentification::layouts.user.form', compact('config', 'data'));
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'roles'         => 'required|array',
            'name'          => 'required',
            'email'         => 'required|email|unique:users,email,' . $request['email'] . ',email',
            'active'        => 'required|between:0,1',
            'image'         => 'nullable|image|mimes:jpeg,png,jpg,gif,svg|max:2024',
        ]);

        if ($validator->passes()) {
            DB::beginTransaction();
            try {
                $data = User::find($id);
                $data->update([
                    'name' => $request['name'],
                    'active' => $request['active'],
                ]);

                $data->roles()->sync($request['roles']);

                if (isset($request->image) && !empty($request->image)) {
                    $image = ImageUpload::profilePicture('image', 'public', $data->image);
                    $data->update(
                        [
                            'image' => $image
                        ]
                    );
                }
                DB::commit();
                $response = response()->json($this->responseStore(true, NULL, route('users.index')));
            } catch (Throwable $throw) {
                // dd($throw);
                DB::rollBack();
                $response = response()->json($this->responseStore(false));
            }
        } else {
            $response = response()->json(['error' => $validator->errors()->all()]);
        }
        return $response;
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy(User $user)
    {
        $response = response()->json([
            'status' => 'error',
            'message' => 'Data gagal dihapus'
        ]);
        DB::beginTransaction();

        try {
            $user->delete();
            $response = response()->json([
                'status' => 'success',
                'message' => 'Data berhasil dihapus'
            ]);
            DB::commit();
        } catch (Throwable $throw) {
            dd($throw);
            $response = response()->json(['error' => $throw]);
        }

        return $response;
    }
}
